package api.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import api.resource.CidadeResource;
import api.resource.ClienteResource;
import api.resource.EnderecoResource;
import api.resource.EstadoResource;

@Component
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        registerEndpoints();
    }
    private void registerEndpoints() {
    	register(EstadoResource.class);
    	register(CidadeResource.class);
    	register(EnderecoResource.class);
    	register(ClienteResource.class);
    }
}