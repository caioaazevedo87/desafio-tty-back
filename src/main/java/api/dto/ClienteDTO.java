package api.dto;

import java.time.LocalDate;
import java.util.List;

public class ClienteDTO {
	private String nome;
	private String email;
	private String cpfCnpj;
	private Short tipoCliente;
	private LocalDate dataNascimento;
	private List<EnderecoDTO> enderecos;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public Short getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(Short tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public List<EnderecoDTO> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<EnderecoDTO> enderecos) {
		this.enderecos = enderecos;
	}

}
