package api.excepction;

public class ApiException extends RuntimeException {
	
	private static final long serialVersionUID = 2834758112464596982L;

	public ApiException(String msg) {
		super(msg);
	}

}
