package api.models;

public enum TipoCliente {
	PESSOA_FISICA((short) 1),
	PESSOA_JURIDICA((short) 2);
	
	public Short tipo;
	
	TipoCliente(Short tipo){
		this.tipo = tipo;
	}

	public int getTipo(){
		return tipo;
	}
	
}