package api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import api.models.Cidade;
import api.models.Estado;

@Repository
public interface CidadeRepository extends JpaRepository<Cidade, Long> {

	public List<Cidade> findByEstado(Estado estado);

}
