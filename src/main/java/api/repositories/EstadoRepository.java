package api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import api.models.Estado;

@Repository
public interface EstadoRepository extends JpaRepository<Estado, Long>{

}
