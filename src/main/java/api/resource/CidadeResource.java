package api.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import api.dto.CidadeDTO;
import api.excepction.ApiException;
import api.models.Cidade;
import api.models.Estado;
import api.repositories.CidadeRepository;
import api.repositories.EstadoRepository;
import api.utils.ApiUtils;

@Component
@Path("/endereco/cidade")
public class CidadeResource {

	private static final String LISTA_VAZIA = "Nenhuma cidade cadastrada!";
	private static final String CIDADE_NAO_LOCALIZADA = "Cidade não localizada!";
	private static final String ESTADO_NAO_LOCALIZADO = "Estado não localizado!";
	private static final String NOME_NAO_INFORMADO = "O preenchimento do Nome da Cidade é obrigatório!";


	private CidadeRepository cidadeRepository;
	private EstadoRepository estadoRepository;

	@Autowired
	public CidadeResource(CidadeRepository cidadeRepository, EstadoRepository estadoRepository) {
		super();
		this.cidadeRepository = cidadeRepository;
		this.estadoRepository = estadoRepository;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll() {
		List<Cidade> lista = cidadeRepository.findAll();
		if (lista.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(LISTA_VAZIA)).build();
		}
		return Response.status(Status.OK).entity(lista).build();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") Long id) {
		try {
			Cidade c = localizarCidade(id);
			return Response.status(Status.OK).entity(c).build();
		} catch(ApiException ae) {
            return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(ae.getMessage())).build();
        } catch(Exception ex) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
        }
	}
	
	@GET
	@Path("/listar-por-estado/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findByEstado(@PathParam("id") Long id) {
		Estado e = ApiUtils.buscarEstadoPorId(estadoRepository, id);
		if (e == null) {
			return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(ESTADO_NAO_LOCALIZADO)).build();
		}
		
		List<Cidade> retorno = cidadeRepository.findByEstado(e);
		if (!retorno.isEmpty()) {
			return Response.status(Status.OK).entity(retorno).build();
		}
		return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(LISTA_VAZIA).append(" Estado: ").append(e.getNome())).build();
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response post(CidadeDTO dto) {
		try {
			validarEstado(dto);
			Cidade c = new Cidade();
			Estado e = ApiUtils.buscarEstadoPorId(estadoRepository, dto.getIdEstado());
			c.setNome(dto.getNome());
			c.setEstado(e);
			cidadeRepository.save(c);
			return Response.status(Status.CREATED).entity(c).build();
		} catch(ApiException ae) {
            return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(ae.getMessage())).build();
        } catch(Exception ex) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
        }
	}
	
	@PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response put(@PathParam("id") Long id, CidadeDTO dto) {
        try {
        	validarEstado(dto);
        	Cidade c = localizarCidade(id);
            c.setNome(dto.getNome());
            c.setId(id);
            c.getEstado().setId(dto.getIdEstado());
            cidadeRepository.save(c);
            return Response.status(Status.OK).entity(c).build();
        } catch(ApiException ae) {
            return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(ae.getMessage())).build();
        } catch(Exception ex) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
        }
        
    }

	@DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id) {
        try {
        	Cidade c = localizarCidade(id);
        	cidadeRepository.delete(c);
            return Response.status(Status.OK).build();
        } catch(ApiException ae) {
            return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(ae.getMessage())).build();
        } catch(Exception ex) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
        }
    }
	
	private Cidade localizarCidade(Long id) {
		Cidade c = ApiUtils.buscarCidadePorId(cidadeRepository, id);
		if(c == null) {
			throw new ApiException(CIDADE_NAO_LOCALIZADA);
        }
		return c;
	}
	
	private void validarEstado(CidadeDTO dto) {
		//REGRA DE NEGÓCIO (F)
		if (dto.getNome() == null) {
			throw new ApiException(NOME_NAO_INFORMADO);
		}
	}
	
}