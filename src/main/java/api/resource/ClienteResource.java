package api.resource;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import api.dto.ClienteDTO;
import api.dto.EnderecoDTO;
import api.excepction.ApiException;
import api.models.Cidade;
import api.models.Cliente;
import api.models.Endereco;
import api.models.TipoCliente;
import api.repositories.CidadeRepository;
import api.repositories.ClienteRepository;
import api.utils.ApiUtils;

@Component
@Path("/cliente")
public class ClienteResource {

	private static final String LISTA_VAZIA = "Nenhum cliente cadastrado!";
	private static final String CLIENTE_NAO_LOCALIZADO = "Cliente não localizado!";
	private static final String CIDADE_NAO_LOCALIZADA = "Nenhum cliente foi salvo. Motivo: A seguinte cidade informada em um dos endereços não foi localizada ";
	private static final String TIPO_CLIENTE_INVALIDO = "Tipo de cliente inválido! Necessário preencher '1' para cliente PF ou '2' para cliente PJ";
	private static final String CPF_INVALIDO = "CPF inválido! O CPF deve conter 11 caracteres.";
	private static final String CNPJ_INVALIDO = "CNPJ inválido! O CNPJ deve conter 14 caracteres.";
	private static final String EMAIL_JA_CADASTRADO = "O e-mail informado já possui cadastrado!";
	private static final String DATA_NASCIMENTO_INVALIDA = "A data de nascimento precisa ser menor que a data atual!";
	private static final String ATRIBUTO_NAO_PREENCHIDO = "Campos obrigatórios: NOME, EMAIL, CPFCNPJ, TIPOCLIENTE, DATANASCIMENTO, ENDERECOS (PELO MENOS 1)";

	private ClienteRepository clienteRepository;
	private CidadeRepository cidadeRepository;

	@Autowired
	public ClienteResource(ClienteRepository clienteRepository, CidadeRepository cidadeRepository) {
		super();
		this.clienteRepository = clienteRepository;
		this.cidadeRepository = cidadeRepository;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll() {
		List<Cliente> lista = clienteRepository.findAll();
		if (lista.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(LISTA_VAZIA)).build();
		}
		return Response.status(Status.OK).entity(lista).build();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") Long id) {
		try {
			Cliente cli = localizarCliente(id);
			return Response.status(Status.OK).entity(cli).build();
		} catch (ApiException ae) {
			return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(ae.getMessage())).build();
		} catch (Exception ex) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
		}
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response post(ClienteDTO dto) {
		try {
			validarClienteIncluir(dto);
			Cliente clienteParaSalvar = new Cliente();
			clienteParaSalvar.setTipo(dto.getTipoCliente() == 1 ? TipoCliente.PESSOA_FISICA : TipoCliente.PESSOA_JURIDICA );
			clienteParaSalvar.setCpfCnpj(dto.getCpfCnpj());
			clienteParaSalvar.setDataNascimento(dto.getDataNascimento());
			clienteParaSalvar.setEmail(dto.getEmail());
			clienteParaSalvar.setNome(dto.getNome());
			clienteParaSalvar.setEnderecos(preencherEnderecosPelaDTO(dto));
			clienteRepository.save(clienteParaSalvar);
			return Response.status(Status.CREATED).entity(clienteParaSalvar).build();
		} catch (ApiException ae) {
			return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(ae.getMessage())).build();
		} catch (Exception ex) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
		}
	}

	@PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response put(@PathParam("id") Long id, ClienteDTO dto) {
		try {
			Cliente clienteParaSalvar = localizarCliente(id);
			validarClienteAlterar(dto);
			if (dto.getCpfCnpj() != null) clienteParaSalvar.setCpfCnpj(dto.getCpfCnpj());
			if (dto.getDataNascimento() != null) clienteParaSalvar.setDataNascimento(dto.getDataNascimento());
			if (dto.getEmail() != null) clienteParaSalvar.setEmail(dto.getEmail());
			if (dto.getEnderecos() != null) clienteParaSalvar.setEnderecos(preencherEnderecosPelaDTO(dto));
			if (dto.getNome() != null) clienteParaSalvar.setNome(dto.getNome());
			if (dto.getTipoCliente() != null) clienteParaSalvar.setTipo(dto.getTipoCliente() == 1 ? TipoCliente.PESSOA_FISICA : TipoCliente.PESSOA_JURIDICA );
			clienteRepository.save(clienteParaSalvar);
			return Response.status(Status.CREATED).entity(clienteParaSalvar).build();
		} catch (ApiException ae) {
			return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(ae.getMessage())).build();
		} catch (Exception ex) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
		}
    }
	
	@DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id) {
		Cliente cli = localizarCliente(id);
        try {
        	clienteRepository.delete(cli);
            return Response.status(Status.OK).build();
        } catch(Exception ex) {
        	return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
        }
    }
	
	private Cliente localizarCliente(Long id) {
		Cliente cli = ApiUtils.buscarClientePorId(clienteRepository, id);
		if (cli == null) {
			throw new ApiException(CLIENTE_NAO_LOCALIZADO);
		}
		return cli;
	}
	
	private List<Endereco> preencherEnderecosPelaDTO(ClienteDTO clienteDTO) {
		List<Endereco> retorno = new ArrayList<Endereco>();
		for (EnderecoDTO dto : clienteDTO.getEnderecos()) {
			Cidade c = ApiUtils.buscarCidadePorId(cidadeRepository, dto.getIdCidade());
			if (c == null) {
				throw new ApiException(CIDADE_NAO_LOCALIZADA + dto.getIdCidade());
			}
			
			Endereco enderecoParaSalvar = new Endereco();
			enderecoParaSalvar.setBairro(dto.getBairro());
			enderecoParaSalvar.setCep(dto.getCep());
			enderecoParaSalvar.setComplemento(dto.getComplemento());
			enderecoParaSalvar.setId(dto.getIdEndereco());
			enderecoParaSalvar.setLogradouro(dto.getLogradouro());
			enderecoParaSalvar.setNumero(dto.getNumero());
			enderecoParaSalvar.setCidade(c);
		}
		
		return retorno;
	}
	
	private void validarClienteIncluir(ClienteDTO dto) {
		//REGRA DE NEGÓCIO (D)
		if (dto.getCpfCnpj() == null || dto.getDataNascimento() == null || dto.getEmail() == null || dto.getEnderecos() == null || dto.getNome() == null || dto.getTipoCliente() == null) {
			throw new ApiException(ATRIBUTO_NAO_PREENCHIDO);
		}

		//REGRA DE NEGÓCIO (A)
		if (clienteRepository.existsByEmail(dto.getEmail())) {
			throw new ApiException(EMAIL_JA_CADASTRADO);
		}

		validarClienteAlterar(dto);
	}

	private void validarClienteAlterar(ClienteDTO dto) {
		if (dto.getTipoCliente() != null && (dto.getTipoCliente() != 1 || dto.getTipoCliente() != 2)) {
			throw new ApiException(TIPO_CLIENTE_INVALIDO);
		}
		
		//REGRA DE NEGÓCIO (B)
		if (dto.getTipoCliente() == 1 && dto.getCpfCnpj().length() != 11) {
			throw new ApiException(CPF_INVALIDO);
		}
		
		//REGRA DE NEGÓCIO (B)
		if (dto.getTipoCliente() == 2 && dto.getCpfCnpj().length() != 14) {
			throw new ApiException(CNPJ_INVALIDO);
		}
		
		//REGRA DE NEGÓCIO (C)
		if (dto.getDataNascimento() != null && !dto.getDataNascimento().isBefore(LocalDate.now())) {
			throw new ApiException(DATA_NASCIMENTO_INVALIDA);
		}
	}
}