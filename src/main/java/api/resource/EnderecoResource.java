package api.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import api.dto.EnderecoDTO;
import api.excepction.ApiException;
import api.models.Cidade;
import api.models.Cliente;
import api.models.Endereco;
import api.repositories.CidadeRepository;
import api.repositories.ClienteRepository;
import api.repositories.EnderecoRepository;
import api.utils.ApiUtils;

@Component
@Path("/endereco/endereco-cliente")
public class EnderecoResource {

	private static final String CLIENTE_NAO_LOCALIZADO = "Cliente não localizado!";
	private static final String ENDERECO_NAO_LOCALIZADO = "Endereço não localizado!";
	private static final String CIDADE_NAO_LOCALIZADA = "Cidade não localizada!";
	private static final String ATRIBUTO_NAO_PREENCHIDO = "Campos obrigatórios: LOGRADOURO, NUMERO, BAIRRO, CEP, IDCIDADE";

	private EnderecoRepository enderecoRepository;
	private ClienteRepository clienteRepository;
	private CidadeRepository cidadeRepository;

	@Autowired
	public EnderecoResource(EnderecoRepository enderecoRepository, ClienteRepository clienteRepository,
			CidadeRepository cidadeRepository) {
		super();
		this.enderecoRepository = enderecoRepository;
		this.clienteRepository = clienteRepository;
		this.cidadeRepository = cidadeRepository;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findEnderecosByClienteId(@PathParam("id") Long id) {
		Cliente cli = ApiUtils.buscarClientePorId(clienteRepository, id);
		if (cli != null) {
			return Response.status(Status.OK).entity(cli.getEnderecos()).build();
		}
		return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(CLIENTE_NAO_LOCALIZADO)).build();
	}

	@POST
	@Path("/{idCliente}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response post(@PathParam("idCliente") Long idCliente, EnderecoDTO dto) {
		try {
			validarEstado(dto);
			Cliente cli = localizarCliente(idCliente);
			Endereco enderecoParaSalvar = preencherEnderecoPelaDTO(dto);
			enderecoRepository.save(enderecoParaSalvar);
			return Response.status(Status.CREATED).entity(cli).build();
		} catch (ApiException ae) {
			return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(ae.getMessage())).build();
		} catch (Exception ex) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
		}
	}

	@PUT
	@Path("/{idCliente}/{idEndereco}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response put(@PathParam("idCliente") Long idCliente, @PathParam("idEndereco") Long idEndereco, EnderecoDTO dto) {
		try {
			validarEstado(dto);
			Cliente cli = localizarCliente(idCliente);
			localizarEnderecoNaListaDeEnderecosDoCliente(cli.getEnderecos(), idEndereco);
			Endereco enderecoParaSalvar = preencherEnderecoPelaDTO(dto);
			enderecoRepository.save(enderecoParaSalvar);
			return Response.status(Status.OK).entity(cli).build();
		} catch (ApiException ae) {
			return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(ae.getMessage())).build();
		} catch (Exception ex) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
		}
	}
	
	private void localizarEnderecoNaListaDeEnderecosDoCliente(List<Endereco> listaEndereco, Long idEndereco) {
		for (Endereco end : listaEndereco) {
			if (end.getId().equals(idEndereco)) {
				return;
			}
		}
		throw new ApiException(ENDERECO_NAO_LOCALIZADO);
	}

	private Cliente localizarCliente(Long idCliente) {
		Cliente cli = ApiUtils.buscarClientePorId(clienteRepository, idCliente);
		if (cli == null) {
			throw new ApiException(CLIENTE_NAO_LOCALIZADO);
		}
		return cli;
	}

	private Endereco preencherEnderecoPelaDTO(EnderecoDTO dto) {
		Cidade c = ApiUtils.buscarCidadePorId(cidadeRepository, dto.getIdCidade());
		if (c == null) {
			throw new ApiException(CIDADE_NAO_LOCALIZADA);
		}

		Endereco enderecoParaSalvar = new Endereco();
		enderecoParaSalvar.setBairro(dto.getBairro());
		enderecoParaSalvar.setCep(dto.getCep());
		enderecoParaSalvar.setComplemento(dto.getComplemento());
		enderecoParaSalvar.setId(dto.getIdEndereco());
		enderecoParaSalvar.setLogradouro(dto.getLogradouro());
		enderecoParaSalvar.setNumero(dto.getNumero());
		enderecoParaSalvar.setCidade(c);
		return enderecoParaSalvar;
	}
	
	private void validarEstado(EnderecoDTO dto) {
		//REGRA DE NEGÓCIO (E)
		if (dto.getBairro() == null || dto.getCep() == null || dto.getIdCidade() == null || dto.getLogradouro() == null || dto.getNumero() == null) {
			throw new ApiException(ATRIBUTO_NAO_PREENCHIDO);
		}
	}

}