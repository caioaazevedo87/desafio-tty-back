package api.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import api.dto.EstadoDTO;
import api.excepction.ApiException;
import api.models.Estado;
import api.repositories.EstadoRepository;
import api.utils.ApiUtils;

@Component
@Path("/endereco/estado")
public class EstadoResource {

	private static final String LISTA_VAZIA = "Nenhuma cidade cadastrada!";
	private static final String ESTADO_NAO_LOCALIZADO = "Estado não localizado!";
	private static final String NOME_NAO_INFORMADO = "O preenchimento do Nome do Estado é obrigatório!";
	
	private EstadoRepository repository;

	@Autowired
	public EstadoResource(EstadoRepository repository) {
		super();
		this.repository = repository;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listarTodosOsEstados() {
		List<Estado> lista = repository.findAll();
		if (lista.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(LISTA_VAZIA)).build();
		}
		return Response.status(Status.OK).entity(lista).build();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarPorId(@PathParam("id") Long id) {
		try {
			Estado e = localizarEstado(id);
			return Response.status(Status.OK).entity(e).build();
		} catch(ApiException ae) {
            return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(ae.getMessage())).build();
        } catch(Exception ex) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
        }
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response post(EstadoDTO dto) {
		try {
			validarEstado(dto);
			Estado estado = new Estado();
			estado.setNome(dto.getNome());
			repository.save(estado);
			return Response.status(Status.CREATED).entity(estado).build();
		} catch(ApiException ae) {
            return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(ae.getMessage())).build();
        } catch(Exception ex) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
        }
	}
	
	@PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response put(@PathParam("id") Long id, EstadoDTO dto) {
        try {
        	validarEstado(dto);
        	Estado e = localizarEstado(id);
            e.setNome(dto.getNome());
            e.setId(id);
            repository.save(e);
            return Response.status(Status.OK).entity(e).build();
        } catch(ApiException ae) {
            return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(ae.getMessage())).build();
        } catch(Exception ex) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
        }
    }
	
	@DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id) {
        try {
        	Estado e = localizarEstado(id);
            repository.delete(e);
            return Response.status(Status.OK).build();
        } catch(ApiException ae) {
            return Response.status(Status.BAD_REQUEST).entity(new StringBuilder(ae.getMessage())).build();
        } catch(Exception ex) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(new StringBuilder(ex.getMessage())).build();
        }
    }

	private Estado localizarEstado(Long id) {
		Estado e = ApiUtils.buscarEstadoPorId(repository, id);
		if(e == null) {
			throw new ApiException(ESTADO_NAO_LOCALIZADO);
        }
		return e;
	}
	
	private void validarEstado(EstadoDTO dto) {
		//REGRA DE NEGÓCIO (F)
		if (dto.getNome() == null) {
			throw new ApiException(NOME_NAO_INFORMADO);
		}
	}
	
}