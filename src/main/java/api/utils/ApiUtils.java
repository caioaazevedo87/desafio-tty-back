package api.utils;

import java.util.Optional;

import api.models.Cidade;
import api.models.Cliente;
import api.models.Estado;
import api.repositories.CidadeRepository;
import api.repositories.ClienteRepository;
import api.repositories.EstadoRepository;

public class ApiUtils {

	public static Estado buscarEstadoPorId(EstadoRepository repository, Long id) {
		Optional<Estado> op = repository.findById(id);
		if (op.isPresent()) {
			return op.get();
		}
		return null;
	}
	
	public static Cidade buscarCidadePorId(CidadeRepository repository, Long id) {
		Optional<Cidade> op = repository.findById(id);
		if (op.isPresent()) {
			return op.get();
		}
		return null;
	}
	
	public static Cliente buscarClientePorId(ClienteRepository repository, Long id) {
		Optional<Cliente> op = repository.findById(id);
		if (op.isPresent()) {
			return op.get();
		}
		return null;
	}
	
}
